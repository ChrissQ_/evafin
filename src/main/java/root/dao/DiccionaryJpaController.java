
package root.dao;

import root.persistence.entities.Diccionary;
import java.io.Serializable;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.EntityNotFoundException;
import javax.persistence.Persistence;
import javax.persistence.Query;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;
import root.dao.exceptions.NonexistentEntityException;

/**
 *
 * @author Astaroth
 */
public class DiccionaryJpaController implements Serializable{
    
     public DiccionaryJpaController(EntityManagerFactory ent) {
        this.ent = ent;
    }
    private EntityManagerFactory ent = Persistence.createEntityManagerFactory("Diccionary_PU");
    
    public DiccionaryJpaController()
    {
        
    }

    public EntityManager getEntityManager() {
        return ent.createEntityManager();
    }

    public void create(Diccionary diccionary) {
        EntityManager ent = null;
        try {
            ent = getEntityManager();
            ent.getTransaction().begin();
            ent.persist(diccionary);
            ent.getTransaction().commit();
        } finally {
            if (ent != null) {
                ent.close();
            }
        }
    }

    public void edit(Diccionary diccionary) throws NonexistentEntityException, Exception {
        EntityManager ent = null;
        try {
            ent = getEntityManager();
            ent.getTransaction().begin();
            diccionary = ent.merge(diccionary);
            ent.getTransaction().commit();
        } catch (Exception ex) {
            String msge = ex.getLocalizedMessage();
            if (msge == null || msge.length() == 0) {
                Integer id = diccionary.getId();
                if (findDiccionario(id) == null) {
                    throw new NonexistentEntityException("The diccionario with id " + id + " no longer exists.");
                }
            }
            throw ex;
        } finally {
            if (ent != null) {
                ent.close();
            }
        }
    }

    public void destroy(Integer id) throws NonexistentEntityException {
        EntityManager ent = null;
        try {
            ent = getEntityManager();
            ent.getTransaction().begin();
            Diccionary diccionary;
            try {
                diccionary = ent.getReference(Diccionary.class, id);
                diccionary.getId();
            } catch (EntityNotFoundException nfEx) {
                throw new NonexistentEntityException("The diccionario with id " + id + " no longer exists.", nfEx);
            }
            ent.remove(diccionary);
            ent.getTransaction().commit();
        } finally {
            if (ent != null) {
                ent.close();
            }
        }
    }

    public List<Diccionary> findDiccionaryEntities() {
        return findDiccionaryEntities(true, -1, -1);
    }

    public List<Diccionary> findDiccionaryEntities(int maxResults, int firstResult) {
        return findDiccionaryEntities(false, maxResults, firstResult);
    }

    private List<Diccionary> findDiccionaryEntities(boolean all, int maxResults, int firstResult) {
        EntityManager ent = getEntityManager();
        try {
            CriteriaQuery crit = ent.getCriteriaBuilder().createQuery();
            crit.select(crit.from(Diccionary.class));
            Query qry = ent.createQuery(crit);
            if (!all) {
                qry.setMaxResults(maxResults);
                qry.setFirstResult(firstResult);
            }
            return qry.getResultList();
        } finally {
            ent.close();
        }
    }

    public Diccionary findDiccionario(Integer id) {
        EntityManager em = getEntityManager();
        try {
            return em.find(Diccionary.class, id);
        } finally {
            em.close();
        }
    }

    public int getDiccionarioCount() {
        EntityManager ent = getEntityManager();
        try {
            CriteriaQuery crit = ent.getCriteriaBuilder().createQuery();
            Root<Diccionary> ruut = crit.from(Diccionary.class);
            crit.select(ent.getCriteriaBuilder().count(ruut));
            Query qry = ent.createQuery(crit);
            return ((Long) qry.getSingleResult()).intValue();
        } finally {
            ent.close();
        }
    }
}
