/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package root.controller;

import java.io.IOException;
import java.util.List;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.ws.rs.client.Client;
import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.client.WebTarget;
import javax.ws.rs.core.MediaType;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;
import root.dao.DiccionaryJpaController;
import root.persistence.entities.Diccionary;


/**
 *
 * @author Astaroth
 */
public class Controller extends HttpServlet {
    
     protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
    }

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        
        DiccionaryJpaController dao = new DiccionaryJpaController();
        final String json;
        String palabra = request.getParameter("palabra");
        System.out.println("Inicio Controller ");
        
        //BEGIN - LLAMADO A API REST LOCAL
        Client client = ClientBuilder.newClient();
        WebTarget myResource = client.target("https://diccionaryprl.herokuapp.com/api/diccionario/" + palabra);
        json = myResource.request(MediaType.APPLICATION_JSON).header("api-key", "fe6e811a-c6c1-41ab-905f-070cdef63ba6").header("api-id", "b657da93").get(String.class);
        //END - LLAMADO A API REST LOCAL

        //Begin - Parseo de Respuesta JSON para obtener significado
        System.out.println("Inicio JSON Parser ");
        JSONParser parser = new JSONParser();
        String ret = "404";
        try
        {
            final String result = json;
            final Object parse = parser.parse(result);
            ret = (String) ((JSONArray) ((JSONObject) ((JSONArray) ((JSONObject) ((JSONArray) ((JSONObject) 
                    ((JSONArray) ((JSONObject) ((JSONArray) ((JSONObject) parse)
                    .get("results")).get(0)).get("lexicalEntries")).get(0)).get("entries"))
                    .get(0)).get("senses")).get(0)).get("definitions")).get(0);
        } catch (ParseException ex) {
            System.out.println(ex);
        }
        System.out.println("resultado desde JSON : " + ret);
        //End - Parseo de Respuesta JSON para obtener significado
        
        //Begin - Insert a BD
        Diccionary dic = new Diccionary();
        dic.setPalabra(palabra);        
        if(!"404".equals(ret)){
            dic.setEstado(Boolean.TRUE);
            dic.setSignificado(ret);
        }
        else{
            dic.setEstado(Boolean.FALSE);
            dic.setSignificado("-- Sin Resultado --");
            ret = dic.getSignificado();
        }        
        dao.create(dic);
        //End - Insert a BD
        
        //Begin - Get Lista de Consultas
        List<Diccionary> dict = dao.findDiccionaryEntities();
        //End - Get Lista de Consultas
        
        //Retorno a Pagina de Resultado
        request.setAttribute("palabra", palabra);
        request.setAttribute("significado", ret);
        request.setAttribute("lista", dict);
        request.getRequestDispatcher("salida.jsp").forward(request, response);
    }

    @Override
    public String getServletInfo() {
        return "Short description";
    }

    private void assertEquals(String string, String property) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    private void assertNull(String property) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }
}
