/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package root.persistence.entities;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bbind.annotation.XmlRootElement;

/**
 *
 * @author Astaroth
 */

@Entity
@Table(name = "diccionary")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Diccionario.findAll", query = "SELECT d FROM Diccionario d"),
    @NamedQuery(name = "Diccionario.findById", query = "SELECT d FROM Diccionario d WHERE d.id = :id"),
    @NamedQuery(name = "Diccionario.findByPalabra", query = "SELECT d FROM Diccionario d WHERE d.palabra = :palabra"),
    @NamedQuery(name = "Diccionario.findBySignificado", query = "SELECT d FROM Diccionario d WHERE d.significado = :significado"),
    @NamedQuery(name = "Diccionario.findByEstado", query = "SELECT d FROM Diccionario d WHERE d.estado = :estado")})
public class Diccionary implements Serializable{
    
    
    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "id")
    private Integer id;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 2147483647)
    @Column(name = "palabra")
    private String palabra;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 2147483647)
    @Column(name = "significado")
    private String significado;
    @Column(name = "estado")
    private Boolean estado;

    public Diccionary() {
    }

    public Diccionary(Integer id) {
        this.id = id;
    }

    public Diccionary(Integer id, String palabra, String significado) {
        this.id = id;
        this.palabra = palabra;
        this.significado = significado;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getPalabra() {
        return palabra;
    }

    public void setPalabra(String palabra) {
        this.palabra = palabra;
    }

    public String getSignificado() {
        return significado;
    }

    public void setSignificado(String significado) {
        this.significado = significado;
    }

    public Boolean getEstado() {
        return estado;
    }

    public void setEstado(Boolean estado) {
        this.estado = estado;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Diccionary)) {
            return false;
        }
        Diccionary other = (Diccionary) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "root.persistence.entities.Diccionario[ id=" + id + " ]";
    }
}
